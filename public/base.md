class: center, middle
# Designing Better Models  

---

##Model Style Ordering

- Если необходимо `Choices` можно опредилиь в моделе. Наименование в верхнем регистре.
- Все поля базы данных.
- Кастомный `manager` или `queryset` (`SomeQuerySet.as_manager()`)
- `class Meta`
- `def __str__()`
- `def save()`
- `def get_absolute_url()`
- все остальные методы


---

Пример:

```python
...
class Company(models.Model):
    # CHOICES
    PUBLIC_LIMITED_COMPANY = 'PLC'
    PRIVATE_COMPANY_LIMITED = 'LTD'
    LIMITED_LIABILITY_PARTNERSHIP = 'LLP'
    COMPANY_TYPE_CHOICES = Choices(
        (PUBLIC_LIMITED_COMPANY, _('Public limited company')),
        (PRIVATE_COMPANY_LIMITED, _('Private company limited by shares')),
        (LIMITED_LIABILITY_PARTNERSHIP, _('Limited liability partnership')),
    )

    # DATABASE FIELDS
    name = models.CharField(_('Name'), max_length=30)
    vat_identification_number = models.CharField(_('VAT'), max_length=20)
    company_type = models.CharField(
        _('Type'), 
        max_length=3, 
        choices=COMPANY_TYPE_CHOICES
    )

    # MANAGERS
    objects = models.Manager()
    limited_companies = LimitedCompanyManager()
    ...
```

---


```python
    ...
    # META CLASS
    class Meta:
        verbose_name = 'company'
        verbose_name_plural = 'companies'

    # TO STRING METHOD
    def __str__(self):
        return self.name

    # SAVE METHOD
    def save(self, *args, **kwargs):
        do_something()
        super().save(*args, **kwargs)  # Call the "real" save() method.
        do_something_else()

    # ABSOLUTE URL METHOD
    def get_absolute_url(self):
        return reverse('company_details', kwargs={'pk': self.id})

    # OTHER METHODS
    def process_invoices(self):
        do_something()

```

---

## Reverse Relationships

related_name:

```python
class Company:
    name = models.CharField(max_length=30)

class Employee:
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    company = models.ForeignKey(
        Company, on_delete=models.CASCADE, related_name='employees'
    )
```

```python
google = Company.objects.get(name='Google')
google.employees.all()
james = Employee.objects.get(first_name='James')
google = Company.objects.get(name='Google')
google.employees.add(james)
```

---

 related_query_name:

```python
companies = Company.objects.filter(employee__first_name='Vitor')
```

```python
class Employee
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    company = models.ForeignKey(
        Company,
        on_delete=models.CASCADE,
        related_name='employees',
        related_query_name='person'
    )
```

```python
companies = Company.objects.filter(person__first_name='Vitor')
```

---

# Blank and Null Fields

- Null: База данных. Определяет, будет ли данный столбец базы данных принимать нулевые значения или нет.
- Blank: Валидация. Будет использоваться при проверке форм и т.п, при вызове `form.is_valid ().`

```python
class Person(models.Model):
    # Mandatory
    name = models.CharField(max_length=255)  
    # Optional (don't put null=True)
    bio = models.TextField(max_length=500, blank=True)  
    # Optional (here you may add null=True)
    birth_date = models.DateField(null=True, blank=True) 
```

---

class: center, middle
# Django generic relations  

---

# Установка

Если вы ничего не удаляли из настроек джанги по умолчанию, то в `INSTALLED_APPS` уже прописанно `'django.contrib.contenttypes'`

```python
# server/app/settings.py

INSTALLED_APPS = [
    ...
    'django.contrib.contenttypes',
	...
]
```

---

Например у нас будет социальное приложение в котором у пользователя будет возможность задавать и отвечать на вопросы,
голосовать за и против, отмечать как избранное, и отмечать как понравившиеся.

Для этого будет модель действия `Activity`:

```python
from django.db import models
from django.utils.translation import ugettext_lazy as _

from model_utils.choices import Choices


# Constance
FAVORITE = "F"
LIKE = "L"
UP_VOTE = "U"
DOWN_VOTE = "D"
ACTIVITY_TYPES = Choices(
    (FAVORITE, _("Favorite")),
    (LIKE, _("Like")),
    (UP_VOTE, _("Up vote")),
    (DOWN_VOTE, _("Down vote")),
)
...
```

---

```python
...
# Activity model
class Activity(models.Model):
    _ACTIVITY_TYPES = ACTIVITY_TYPES

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, verbose_name=_("User"),
        on_delete=models.CASCADE, related_name="activity",
    )
    activity_type = models.CharField(
        verbose_name=_("Activity type"), max_length=1, choices=_ACTIVITY_TYPES
    )
    created_at = models.DateTimeField(
        verbose_name=_("Created at"), auto_now_add=True
    )
    post = models.ForeignKey(
        Post, verbose_name=_("Post"), on_delete=models.CASCADE, null=True
    )
    question = models.ForeignKey(
        Question, verbose_name=_("Question"), on_delete=models.CASCADE, null=True
    )
    answer = models.ForeignKey(
        Answer, verbose_name=_("Answer"), on_delete=models.CASCADE, null=True
    )
    comment = models.ForeignKey(
        Comment, verbose_name=_("Comment"), on_delete=models.CASCADE, null=True
    )

    class Meta:
        verbose_name = _("Activity")
        verbose_name_plural = _("Activities")

```

---

Для того чтобы проголосовать пользователю за вопрос с ID 64:


```python
Activity.objects.create(user=request.user, activity_type=UP_VOTE, question=64)
```

Для того чтобы подсчитать кол-во голосов 'за' нужно:

```python
question = Question.objects.get(pk=64)
up_votes = question.activity_set.filter(activity_type=UP_VOTE)

# Кол-во голосов "за"
count = up_votes.count()

# Имена тех кто проголосовал "за"
up_voters = up_votes.values_list('user__first_name', flat=True)
```

---

# Примет с использованием generic relations

Тот же пример используя `generic relations`

Модели:


```python 
...
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
...
# Activity model
class Activity(models.Model):
    _ACTIVITY_TYPES = ACTIVITY_TYPES
    # Не измененные поля
    user = ...
    activity_type = ...
    created_at = ...

    # Поля определяющие generic relation
    content_type = models.ForeignKey(
        ContentType, on_delete=models.CASCADE
    )
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey()

    ...
```

---

Для удобного доступа к такого рода 'динамической' связи необходимо определить дополнительные поля в моделях `Post`, `Question`, `Answer`, `Comment`:


```python 
from django.db import models
from django.contrib.contenttypes.fields import GenericRelation

from activities.models import Activity


class Post(models.Model):
    ...
    likes = GenericRelation(Activity)
    ...


class Question(models.Model):
    ...
    activities = GenericRelation(Activity)
    ...


class Answer(models.Model):
    ...
    votes = GenericRelation(Activity)
    ...


class Comment(models.Model):
    ...
    likes = GenericRelation(Activity)
    ...

```

---

Теперь для того чтобы "лайкнуть" пост необходимо:

```python 
# Получить пост
post = Post.objects.get(id=64)

# Создать событие лайка
post.likes.create(
    activity_type=LIKE, 
    user=request.user
)
# ---- ИЛИ ----
Activity.objects.create(
    content_object=post, 
    activity_type=LIKE, 
    user=request.user
)

# Получить все лайки данного поста
post.likes.all()

# Получить кол-во лайков данного поста
post.likes.count()

# Получить имена пользователей, которые лайкнули пост
post.likes.values_list("user__first_name", flat=True)
```

---

# Дополнительно

Если вам необходимо добавить новую модель для взаимодействия, требуется всего лишь определить `GenericRelation`:

```python
from django.contrib.contenttypes.fields import GenericRelation
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Picture(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL
        verbose_name=_("User"),
        on_delete=models.CASCADE,
        related_name="user",
    )
    image = models.ImageField(
        verbose_name=_("Image"),
    )
    created_at = models.DateTimeField(
        verbose_name=_("Created at"),
        auto_now_add=True
    )
    favorites = GenericRelation(Activity)

```

---

И сразу же можно использовать

```python

picture = Picture.objects.get(pk=1)

# Mark picture as favorite
picture.favorites.create(
    activity_type=Activity.FAVORITE, 
    user=request.user
)

# Get favorites amount
picture.favorites.count()

```

---

# Обратная связь

Для определения обратной связи:

```python 
class Picture(models.Model):
    ...
    favorites = GenericRelation(Activity, related_query_name='pictures')
    ...
```

И теперь обратный доступ к модели `Picture`:

```python
# Получить собития связаные с пользователем и картинками
picture_activities = Activity.objects.filter(pictures__user=request.user)
# Отмеченые в избраное картинки
favorites = picture_activities.filter(activity_type=FAVORITE)
# Их кол-во
favorites.count()
```

---

# Предостережения

Это только один из способов использования `ContentTypes` и `GenericRelations`. 

Несмотря на то, что он кажется очень приятным в использовании, будьте осторожны при его реализации! 
Это добавляет дополнительный уровень сложности и в конечном итоге сделает некоторые механизмы медленнее. 

Еще одно предостережение: 
`GenericForeignKey` не принимает аргумент `on_delete` для настройки этого поведения. Поведение по умолчанию будет каскадировать все отношения.


---

Один из способов избежать поведения по умолчанию - не определять `GenericRelation`:


```python
class Comment(models.Model):
    text = models.TextField(
        verbose_name=_("Text")
    )
    created_at = models.DateTimeField(
        verbose_name=_("Created at"),
        auto_now_add=True
    )

# Создаем комментарий
comment = Comment.objects.create(text='This is a test comment')

# Отмечаем его как понравившийся
Activity.objects.create(
    content_object=comment, 
    activity_type=Activity.LIKE, 
    user=request.user
)
```

---

Теперь, чтобы получить список лайков этого комментария, вы должны использовать класс `ContentType`


```python
from django.contrib.contenttypes.models import ContentType

# Pass the instance we created in the snippet above
ct = ContentType.objects.get_for_model(comment)

# Get the list of likes
comment_likes = Activity.objects.filter(
    content_type=ct, 
    object_id=comment.id, 
    activity_type=Activity.LIKE
)
```

Это также вариант, если вы хотите взаимодействовать с моделью из модуля contrib Django или любой сторонней моделью, к которой у вас нет доступа для добавления GenericRelation.
